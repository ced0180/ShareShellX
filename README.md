# ShareShellX - An uploader that can be used from the command line.

To use it, you need [xfce4-screenshooter](https://docs.xfce.org/apps/screenshooter/start), [zenity](https://wiki.gnome.org/Projects/Zenity), [aplay](https://linux.die.net/man/1/aplay), [notify-send](https://manpages.ubuntu.com/manpages/xenial/man1/notify-send.1.html) and [curl](https://curl.se/).

## command
```
 * shareshellx <-w | --current-window> - Take a picture of the active window.
 * shareshellx <-f | --fullscreen>     - Take pictures of all the screens.
 * shareshellx <-r | --select-region>  - Takes a picture of the specified area.
 * shareshellx <-s | --select-file>    - Upload videos and other images that you have always taken.
```

<3 Thanks to my friend who helped me create the tool.
